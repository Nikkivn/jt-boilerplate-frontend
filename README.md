# Boilerplate - distribution build

Source and distribution build for Boilerplate

## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone https://bitbucket.org/Nikkivn/jt-boilerplate-frontend
```

```bash
cd jt-boilerplate-frontend/
```

```bash
npm install && bower install
```

While you're working on your project, run:

`grunt`

To build a 'distribution' ready version (unminified JS), run:

`grunt publish`

To build a 'distribution' ready version WITH minified JS, run:

`grunt publish --minify`

And you're set!

## Directory Structure
  * `src/` : main source folder
  * `src/scss/_settings.scss`: Configuration settings
  * `src/scss/src.scss`: srclication styles
  * `src/js/custom`: Custom plugins written specifically for this project
  * `src/js/src.js`: Main src JS file. Insert custom JS here if required
  * `src/js/src-ie.js`: IE specific JS
  * `src/js/vendor`: Vendor plugins.

## Important Notes

Only edit source files in the `src/` folder. Changes made here are tracked by the repository.


```>
grunt publish
```

will create/overwrite a `dist/` folder which contains the distribution ready code.